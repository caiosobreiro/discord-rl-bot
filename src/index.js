const Discord = require('discord.js')
const axios = require('axios')

const bot = new Discord.Client()

async function processCommand (message) {
    const [activation, command, playerId] = message.content.split(' ')
    console.log(message.content)

    if (activation !== 'rl') return

    switch (command) {
        case 'help':
            await showHelp(message)
            break
        case 'ranks':
            await showRanks(playerId, message)
            break
        default:
            await message.channel.send('Que?')
    }
}

async function showHelp (message) {
    const string = [
        '**Commands:**',
        '`help`: Show this help',
        '`ranks [playerId]`: Show the ranks for a given player'
    ].join('\n')
    return message.channel.send(string)
}

async function showRanks (playerId, message) {

    if (!playerId) {
        return message.channel.send('PlayerId is required')
    }

    const {data: profile} = await axios.get(`https://api.tracker.gg/api/v2/rocket-league/standard/profile/steam/${playerId}`)
    const ranks = profile.data.segments
    const string = ranks.map(rank => {
        let playlist
        try {
            playlist = rank.metadata.name
        } catch (err) {
            playlist = '-'
        }
        let rankName, rankValue, winStreak, winStreakEmoji
        try {
            rankName = rank.stats.tier.metadata.name
            rankValue = rank.stats.tier.value
            winStreak = rank.stats.winStreak.displayValue
            winStreakEmoji = winStreak < 0 ? '😢' : winStreak < 2 ? '😀' : '🔥'
        } catch (err) {
            rankName = '-'
            rankValue = 0
            winStreak = '-'
        }
        return `${playlist}: ${rankName} - Win Streak: ${winStreak} ${winStreakEmoji || ''}`
    })
    .join('\n')
    return message.channel.send(`Ranks for player ${profile.data.platformInfo.platformUserHandle}\n\n${string}`)
}

bot.on('message', processCommand)

// login with bot token
bot.login('NzcwNDMxODU1MzU3ODUzNzE2.X5delA.lXWijdILWFAJkUKXTxpfacrQWQQ')